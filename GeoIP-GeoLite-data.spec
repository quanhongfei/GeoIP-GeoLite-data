Name:      GeoIP-GeoLite-data
#No further releases of IPv4 GeoLite Legacy databases will be made from April 2018.

Version:   2018.06
Release:   3
Summary:   Free GeoLite IP geolocation country database
License:   CC-BY-SA
URL:       http://dev.maxmind.com/geoip/legacy/geolite/
Source0:   http://geolite.maxmind.com/download/geoip/database/GeoLiteCountry/GeoIP.dat.gz
Source1:   http://geolite.maxmind.com/download/geoip/database/GeoIPv6.dat.gz
Source2:   http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz
Source3:   http://geolite.maxmind.com/download/geoip/database/GeoLiteCityv6-beta/GeoLiteCityv6.dat.gz
Source4:   http://download.maxmind.com/download/geoip/database/asnum/GeoIPASNum.dat.gz
Source5:   http://download.maxmind.com/download/geoip/database/asnum/GeoIPASNumv6.dat.gz
BuildArch: noarch

Obsoletes: GeoIP-GeoLite-data-extra GeoIP-data < 1.6.4-10 geoip-geolite < %{version}
Provides:  GeoIP-GeoLite-data-extra GeoIP-data = %{version} geoip-geolite = %{version}
Conflicts: GeoIP < 1.6.4-3

%description
The GeoLite databases are free IP geolocation databases. This package contains
databases that are free and map IPv4 and IPv6 addresses to countries, cities or
autonomous system number. This product includes GeoLite data created by MaxMind.
Available from
http://www.maxmind.com/

%prep
%setup -q -T -c
install -p -m 644 %{SOURCE0} GeoLiteCountry.dat.gz;gunzip GeoLiteCountry.dat
install -p -m 644 %{SOURCE1} GeoIPv6.dat.gz;       gunzip GeoIPv6.dat
install -p -m 644 %{SOURCE2} GeoLiteCity.dat.gz;   gunzip GeoLiteCity.dat
install -p -m 644 %{SOURCE3} GeoLiteCityv6.dat.gz; gunzip GeoLiteCityv6.dat
install -p -m 644 %{SOURCE4} GeoLiteASNum.dat.gz;  gunzip GeoLiteASNum.dat
install -p -m 644 %{SOURCE5} GeoIPASNumv6.dat.gz;  gunzip GeoIPASNumv6.dat

%build

%install
mkdir -p $RPM_BUILD_ROOT%{_datadir}/GeoIP/
install -p -m 644 GeoLite{Country,ASNum}.dat $RPM_BUILD_ROOT%{_datadir}/GeoIP/
install -p -m 644 GeoLiteCity{,v6}.dat $RPM_BUILD_ROOT%{_datadir}/GeoIP/
install -p -m 644 GeoIP{ASNumv6,v6}.dat   $RPM_BUILD_ROOT%{_datadir}/GeoIP/

# Add symlinks for databases to make themsl compatible with upstream database
# names used in old package
ln -sf GeoLiteASNum.dat $RPM_BUILD_ROOT%{_datadir}/GeoIP/GeoIPASNum.dat
ln -sf GeoIPASNumv6.dat $RPM_BUILD_ROOT%{_datadir}/GeoIP/GeoLiteASNumv6.dat
ln -sf GeoLiteCity.dat $RPM_BUILD_ROOT%{_datadir}/GeoIP/GeoIPCity.dat
ln -sf GeoLiteCityv6.dat $RPM_BUILD_ROOT%{_datadir}/GeoIP/GeoIPCityv6.dat

%preun
if [ $1 = 0 ]; then
  if [ -h %{_datadir}/GeoIP/GeoIP.dat ]; then
    geoipdat=`readlink %{_datadir}/GeoIP/GeoIP.dat`
    if [ "$geoipdat" = "GeoLiteCountry.dat" ]; then
      rm -f %{_datadir}/GeoIP/GeoIP.dat
    fi
  fi
fi
exit 0

%posttrans
if [ ! -e %{_datadir}/GeoIP/GeoIP.dat ]; then
  if [ -e %{_datadir}/GeoIP/GeoIP.dat.rpmsave ]; then
    mv %{_datadir}/GeoIP/GeoIP.dat.rpmsave \
      %{_datadir}/GeoIP/GeoIP.dat
  else
    ln -sf GeoLiteCountry.dat %{_datadir}/GeoIP/GeoIP.dat
  fi
fi
exit 0

%files
%dir %{_datadir}/GeoIP/
%verify(not md5 size mtime) %{_datadir}/GeoIP/*.dat
%{_datadir}/GeoIP/GeoLite{Country,City{,v6},ASNum}.dat
%{_datadir}/GeoIP/GeoIPASNumv6.dat

%changelog
* Wed Sep 25 2019 Alex Chao <zhaolei746@huawei.com> Cynthia Xie <xiehong@huawei.com> - 2018.06-3
- Package init
